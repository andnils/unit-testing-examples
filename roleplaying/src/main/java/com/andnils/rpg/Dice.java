package com.andnils.rpg;

import java.util.Random;

public class Dice {

	public static int D4() {
		return roll(4);
	}

	public static int D6() {
		return roll(6);
	}

	public static int D8() {
		return roll(8);
	}

	public static int D10() {
		return roll(10);
	}

	public static int D12() {
		return roll(12);
	}

	public static int D20() {
		return roll(20);
	}

	public static int D100() {
		return roll(100);
	}

	private static int roll(int i) {
		Random random = new Random();
		return 1 + random.nextInt(i);
	}
}
