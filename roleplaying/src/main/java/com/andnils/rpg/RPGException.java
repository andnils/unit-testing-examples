package com.andnils.rpg;

public class RPGException extends Exception {

	public RPGException()  {
		super();
	}

	public RPGException(String msg) {
		super(msg);
	}
}
