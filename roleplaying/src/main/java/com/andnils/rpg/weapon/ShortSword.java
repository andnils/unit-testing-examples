package com.andnils.rpg.weapon;

import com.andnils.rpg.Dice;

public class ShortSword implements Weapon {

	@Override
	public String getName() {
		return "ShortSword";
	}


	@Override
	public int getDamage() {
		return Dice.D4() + 2;
	}


	@Override
	public int getWeight() {
		return 4;
	}
}
