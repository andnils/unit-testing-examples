package com.andnils.rpg.weapon;

import com.andnils.rpg.Equipment;

public interface Weapon extends Equipment {

	String getName();
	int getDamage();

}
