package com.andnils.rpg.weapon;

import java.util.Random;

import com.andnils.rpg.Dice;

public class LongSword implements Weapon {

	private Random dice = new Random();


	@Override
	public String getName() {
		return "LongSword";
	}


	@Override
	public int getDamage() {
		return Dice.D8() + 2;
	}


	@Override
	public int getWeight() {
		return 9;
	}

}
