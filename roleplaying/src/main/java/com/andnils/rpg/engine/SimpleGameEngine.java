package com.andnils.rpg.engine;

import java.util.Collection;

import com.andnils.rpg.Dice;
import com.andnils.rpg.character.Enemy;
import com.andnils.rpg.character.Player;

public class SimpleGameEngine implements GameEngine {

	@Override
	public Outcome process(Player player, Collection<Enemy> enemies) {

		Outcome outcome;

		int roll = Dice.D10() + player.getLevel();

		if (roll > enemies.size()) {
			outcome = Outcome.ENEMIES_FLEE;
		} else {
			outcome = Outcome.ENEMIES_ATTACK;
		}

		return outcome;
	}
}
