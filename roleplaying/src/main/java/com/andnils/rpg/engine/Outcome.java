package com.andnils.rpg.engine;

public enum Outcome {

	ENEMIES_ATTACK,
	ENEMIES_FLEE;
}
