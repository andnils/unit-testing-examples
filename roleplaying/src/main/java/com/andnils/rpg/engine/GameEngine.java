package com.andnils.rpg.engine;

import java.util.Collection;

import com.andnils.rpg.character.Enemy;
import com.andnils.rpg.character.Player;

public interface GameEngine {

	Outcome process(Player player, Collection<Enemy> enemies);
}
