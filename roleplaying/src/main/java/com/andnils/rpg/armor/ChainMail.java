package com.andnils.rpg.armor;

public class ChainMail implements Armor {

	@Override
	public String getName() {
		return "ChainMail";
	}


	@Override
	public int getPoints() {
		return 5;
	}


	@Override
	public int getWeight() {
		return 20;
	}
}
