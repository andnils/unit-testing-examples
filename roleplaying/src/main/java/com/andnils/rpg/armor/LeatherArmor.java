package com.andnils.rpg.armor;

public class LeatherArmor implements Armor {

	@Override
	public String getName() {
		return "Leather";
	}


	@Override
	public int getPoints() {
		return 2;
	}


	@Override
	public int getWeight() {
		return 8;
	}
}
