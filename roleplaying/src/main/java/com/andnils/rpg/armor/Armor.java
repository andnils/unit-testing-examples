package com.andnils.rpg.armor;

import com.andnils.rpg.Equipment;

public interface Armor extends Equipment {

	String getName();
	int getPoints();
}
