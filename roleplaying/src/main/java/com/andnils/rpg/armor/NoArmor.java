package com.andnils.rpg.armor;

public class NoArmor implements Armor {

	@Override
	public String getName() {
		return "NoArmor";
	}


	@Override
	public int getPoints() {
		return 0;
	}


	@Override
	public int getWeight() {
		return 0;
	}
}
