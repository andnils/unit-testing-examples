package com.andnils.rpg.character;

import com.andnils.rpg.RPGException;
import com.andnils.rpg.armor.Armor;
import com.andnils.rpg.armor.NoArmor;
import com.andnils.rpg.weapon.Weapon;

public abstract class Character {

	protected int maxHitPoints;
	protected int currentHitPoints;
	protected int level;
	protected Weapon weapon;
	protected int weaponSkill;
	protected Armor armor;



	public int getCurrentHitPoints() {
		return currentHitPoints;
	}


	public void damage(int damage) throws CharacterDiedException {
		int resultingDamage = damage - getArmor().getPoints();
		if (resultingDamage > 0) {
			this.currentHitPoints = currentHitPoints - damage;
		}
		if (currentHitPoints < 1) {
			throw new CharacterDiedException();
		}
	}


	public Weapon getWeapon() {
		return weapon;
	}


	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}


	public Armor getArmor() {
		if (armor == null) armor = new NoArmor();
		return armor;
	}


	public void setArmor(Armor armor) {
		this.armor = armor;
	}

	public int getLevel() {
		return level;
	}


	public void setLevel(int level) {
		this.level = level;
	}


	public int getWeaponSkill() {
		return weaponSkill;
	}


	public void setWeaponSkill(int weaponSkill) {
		this.weaponSkill = weaponSkill;
	}


	public void levelUp() throws RPGException {
		this.level++;
		this.maxHitPoints = maxHitPoints + 5;
		heal();
	}

	public boolean isDead() {
		return (currentHitPoints < 1);
	}

	public void heal() throws RPGException {
		if (isDead()) {
			throw new RPGException("Cannot heal dead character");
		}
		currentHitPoints = maxHitPoints;
	}

	public void attack(Character opponent) {


	}


	public int getMaxHitPoints() {
		return maxHitPoints;
	}
}
