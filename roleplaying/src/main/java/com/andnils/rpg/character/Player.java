package com.andnils.rpg.character;

import com.andnils.rpg.armor.NoArmor;

public class Player extends Character {

	private final String name;


	public Player(String name, int level, int weaponSkill, int hitPoints) {
		this.name = name;
		this.level = level;
		this.weaponSkill = weaponSkill;
		this.maxHitPoints = hitPoints;
		this.currentHitPoints = hitPoints;
		this.armor = new NoArmor();
	}


	public String getName() {
		return name;
	}

}
