package com.andnils.rpg.character;

import com.andnils.rpg.Dice;
import com.andnils.rpg.armor.Armor;
import com.andnils.rpg.weapon.Weapon;

public class CharacterBuilder {

	private String name;
	private int hitPoints;
	private Armor armor;
	private Weapon weapon;
	private int level;
	private int weaponSkill;

	public CharacterBuilder() {
		this.hitPoints = Dice.D10();
		this.name = String.format("RandomPlayer-%03d", Dice.D100());
	}


	public CharacterBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public CharacterBuilder withHitPoints(int hitPoints) {
		this.hitPoints = hitPoints;
		return this;
	}

	public CharacterBuilder withLevel(int level) {
		this.level = level;
		return this;
	}

	public CharacterBuilder withWeaponSkill(int skill) {
		this.weaponSkill = skill;
		return this;
	}

	public CharacterBuilder withArmor(Armor armor) {
		this.armor = armor;
		return this;
	}

	public CharacterBuilder withWeapon(Weapon weapon) {
		this.weapon = weapon;
		return this;
	}


	public Player buildPlayer() {
		Player player = new Player(name, level, weaponSkill, hitPoints);
		player.setWeapon(weapon);
		player.setArmor(armor);
		return player;
	}

	public Enemy buildEnemy() {
		Enemy enemy = new Enemy(level, weaponSkill, hitPoints);
		enemy.setWeapon(weapon);
		enemy.setArmor(armor);
		return enemy;
	}
}