package com.andnils.rpg.character;

import com.andnils.rpg.armor.NoArmor;

public class Enemy extends Character {

	public Enemy(int level, int weaponSkill, int hitPoints) {
		this.level = level;
		this.weaponSkill = weaponSkill;
		this.maxHitPoints = hitPoints;
		this.currentHitPoints = hitPoints;
		this.armor = new NoArmor();
	}
}
