Feature: AI driven oppenents

  Scenario: player encounters single, unarmed enemy
    Given the player is out walking
    And equipped with a long sword
    When a goblin appears
    Then the goblin flee

  Scenario: player encounters many enemies
    Given the player is out walking
    And equipped with a short sword
    When a gang of 8 goblins appear
    Then the goblins attack
