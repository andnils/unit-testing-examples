package com.andnils.rpg;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.LinkedList;
import java.util.List;

import com.andnils.rpg.armor.LeatherArmor;
import com.andnils.rpg.character.Enemy;
import com.andnils.rpg.character.Player;
import com.andnils.rpg.character.CharacterBuilder;
import com.andnils.rpg.engine.SimpleGameEngine;
import com.andnils.rpg.engine.GameEngine;
import com.andnils.rpg.engine.Outcome;
import com.andnils.rpg.weapon.LongSword;
import com.andnils.rpg.weapon.ShortSword;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EnemyEncounterTest {

	private Player player;
	private List<Enemy> enemies;
	private GameEngine processor;


	@Before
	public void init() {
		enemies = new LinkedList<>();
		processor = new SimpleGameEngine();
	}


	@Given("^the player is out walking")
	public void the_player_is_alone() throws Throwable {

		player = new CharacterBuilder()
				.withName("Legolas")
				.withHitPoints(10)
				.buildPlayer();
	}


	@And("^equipped with a long sword$")
	public void equipped_with_a_long_sword() throws Throwable {
		player.setWeapon(new LongSword());
	}


	@And("^equipped with a short sword$")
	public void equipped_with_a_short_sword() throws Throwable {
		player.setWeapon(new ShortSword());
	}


	@When("^a goblin appears$")
	public void a_goblin_appears() throws Throwable {
		enemies.add(newGoblin());
	}


	@When("^a gang of (\\d+) goblins appear$")
	public void a_gang_of_goblins_appear(int numEnemies) throws Throwable {
		for (int i = 0; i < numEnemies; i++) {
			enemies.add(newGoblin());
		}

	}


	@Then("^the goblin flee$")
	public void the_goblin_flee() throws Throwable {
		Outcome process = processor.process(player, enemies);

		assertThat(process, equalTo(Outcome.ENEMIES_FLEE));
	}


	@Then("^the goblins attack$")
	public void the_goblins_attack() throws Throwable {
		Outcome process = processor.process(player, enemies);

		assertThat(process, equalTo(Outcome.ENEMIES_ATTACK));
	}


	private Enemy newGoblin() {
		return new CharacterBuilder()
				.withWeapon(new ShortSword())
				.withArmor(new LeatherArmor())
				.withHitPoints(8)
				.withLevel(2)
				.withWeaponSkill(3)
				.buildEnemy();
	}
}
