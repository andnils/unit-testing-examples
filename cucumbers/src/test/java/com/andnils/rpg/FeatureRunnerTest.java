package com.andnils.rpg;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features")
public class FeatureRunnerTest {
	// Note: class name must in with suffix 'Test'
	// otherwise 'mvn test' will not find it.
}
