package com.andnils.rpg;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import com.andnils.rpg.character.*;
import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

	private CharacterBuilder newPlayer;


	@Before
	public void setup() {
		newPlayer = new CharacterBuilder();
	}


	@Test
	public void hitpoints_decrease_when_damaged() throws CharacterDiedException {
		// Arrange
		Player frodo = newPlayer
				.withHitPoints(2)
				.buildPlayer();

		// Act
		frodo.damage(1);

		// Assert
		assertThat(frodo.getCurrentHitPoints(), equalTo(1));
	}


	@Test(expected = CharacterDiedException.class)
	public void player_dies_when_hitpoints_reach_zero() throws CharacterDiedException {
		Player gandalf = newPlayer
				.withHitPoints(1)
				.buildPlayer();

		gandalf.damage(1);
	}


	@Test
	public void hitpoints_restored_when_healed() throws RPGException {
		Player boromir = newPlayer
				.withHitPoints(10)
				.buildPlayer();

		boromir.damage(5);
		boromir.heal();

		assertThat(boromir.getCurrentHitPoints(), equalTo(boromir.getMaxHitPoints()));
	}

}
