# unit-testing-examples

These modules demonstrates some unit testing features.
Prerequisites: Java 8 with Maven.


## roleplaying

This module contains domain objects for a role playing game engine.
These domain objects are used by the modules below.
In other words, there are no tests to be found here.

## simple-unittest

Demonstrates some base JUnit tests for the roleplaying module.

## cucumbers

Demonstrates BDD-style testing / feature-style testing.

## simple-mocking

Mocking means dependencies, and dependencies means databases :-)
This is a spring based project. This time it is not a fantasy hack-n-slash setup.
Instead we model a car rental service.

Demonstrates uses of the mockito framework.

Uses an in-memory HSQLDB as data source.
