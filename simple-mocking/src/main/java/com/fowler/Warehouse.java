package com.fowler;

public interface Warehouse {

	void add(String item, int qty);

	int getInventory(String item);

	boolean hasInventory(String item, int qty);

	void remove(String item, int qty);
}
