package com.fowler;

import java.util.HashMap;
import java.util.Map;

public class WarehouseImpl implements Warehouse {

	Map<String, Integer> inventory;


	public WarehouseImpl() {
		inventory = new HashMap<>();
	}


	@Override
	public void add(String item, int qty) {
		inventory.put(item, qty);
	}


	@Override
	public int getInventory(String item) {
		return inventory.get(item);
	}


	@Override
	public boolean hasInventory(String item, int qty) {
		return inventory.containsKey(item) && inventory.get(item) >= qty;
	}


	@Override
	public void remove(String item, int qty) {
		if (inventory.containsKey(item)) {
			int newInventory = inventory.get(item) - qty;
			inventory.put(item, newInventory);
		}
	}
}
