package com.fowler;

public class Order {
	private String item;
	private int qty;
	private boolean filled;

	public Order(String item, int qty) {
		this.item = item;
		this.qty = qty;
		this.filled = false;
	}


	public void fill(Warehouse warehouse) {
		if (warehouse.hasInventory(item, qty)) {
			warehouse.remove(item, qty);
			this.filled = true;
		}
	}


	public boolean isFilled() {

		return filled;
	}
}
