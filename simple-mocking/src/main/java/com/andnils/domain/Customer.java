package com.andnils.domain;

import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;

public class Customer {

    private UUID uuid;
    private String name;

    public Customer(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

}
