package com.andnils.domain;

public class Car {

	private String licensePlate;
	private String model;
	private int bonusClass;


	public Car(String licensePlate, String model) {
		this(licensePlate, model, 1);
	}


	public Car(String licensePlate, String model, int bonusClass) {
		this.licensePlate = licensePlate;
		this.model = model;
		this.bonusClass = bonusClass;
	}


	public String getLicensePlate() {
		return licensePlate;
	}


	public String getModel() {
		return model;
	}


	public int getBonusClass() {
		return bonusClass;
	}


	@Override
	public String toString() {
		return String.format("%s (%s)", licensePlate, model);
	}
}
