package com.andnils.domain;

import java.time.LocalDate;

public class Rental {

    private LocalDate fromDate;
    private LocalDate toDate;
    private Customer customer;
    private Car car;

    public Rental(Customer customer, Car car, LocalDate fromDate, LocalDate toDate) {
        this.customer = customer;
        this.car = car;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Car getCar() {
        return car;
    }

    public LocalDate getFromDate() {
        return fromDate;
    }

    public LocalDate getToDate() {
        return toDate;
    }
}
