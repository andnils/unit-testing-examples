package com.andnils.domain;

public enum CarCategory {
    CHEAP,
    STANDARD,
    DELUXE;
}
