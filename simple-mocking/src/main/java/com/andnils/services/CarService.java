package com.andnils.services;

import java.util.List;

import com.andnils.domain.Car;
import com.andnils.repositories.CarDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("CarService")
public class CarService {

	private CarDao carDao;


	@Autowired
	public void setCarDao(CarDao carDao) {
		this.carDao = carDao;
	}


	/**
	 * Find all the Cars we own.
	 *
	 * @return List of Cars
	 */
	public List<Car> findAllCars() {
		return carDao.findAllCars();
	}


	/**
	 * Find a specific Car.
	 *
	 * @param license the license plate, e.g. "ABC-123"
	 * @return the Car with given license plate
	 */
	public Car findByLicensePlate(String license) {
		return carDao.findByLicensePlate(license);
	}

}
