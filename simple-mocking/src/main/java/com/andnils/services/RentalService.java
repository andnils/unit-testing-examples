package com.andnils.services;

import static com.google.common.collect.Sets.newHashSet;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.andnils.domain.Car;
import com.andnils.domain.Rental;
import com.andnils.repositories.CarDao;
import com.andnils.repositories.RentalDao;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("RentalService")
public class RentalService {

	private RentalDao rentalDao;
	private CarDao carDao;


	@Autowired
	public void setRentalDao(RentalDao rentalDao) {
		this.rentalDao = rentalDao;
	}


	@Autowired
	public void setCarDao(CarDao carDao) {
		this.carDao = carDao;
	}


	/**
	 * Find all rentals registered in the database.
	 *
	 * @return List of rentals.
	 */
	public List<Rental> findAllRentals() {
		return rentalDao.findAllPopulatedRentals();
	}


	/**
	 * Find out which cars are available (i.e. not already rented)
	 * on a certain date.
	 *
	 * @param theDate day of rental
	 * @return List of cars available for rent on date.
	 */
	public Set<Car> findAvailableCarsOnDate(LocalDate theDate) {
		// Some of this logic could be performed by DB
		// but we want to do processing in this method.

		Set<Car> allCars = newHashSet(carDao.findAllCars());

		Set<Car> unavailableCars = findAllRentals().stream()
				.filter(rental ->
								theDate.isAfter(rental.getFromDate()) && theDate.isBefore(rental.getToDate())
				)
				.map(Rental::getCar)
				.collect(Collectors.toSet());

		return Sets.difference(allCars, unavailableCars);
	}


	public long calculateBonusPoints(Rental rental) {

		int bonusClass = carDao.findBonusClassForCar(rental.getCar());

		LocalDate fromDate = rental.getFromDate();
		LocalDate toDate = rental.getToDate();
		long days = fromDate.until(toDate, ChronoUnit.DAYS);

		long weeklyExtraPoints = (bonusClass * 100) * (days / 7);

		return (days * bonusClass * 100) + weeklyExtraPoints;
	}
}
