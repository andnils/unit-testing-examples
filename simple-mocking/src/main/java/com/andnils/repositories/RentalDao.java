package com.andnils.repositories;

import java.util.List;

import com.andnils.domain.Rental;


public interface RentalDao {

	List<Rental> findAllPopulatedRentals();
}
