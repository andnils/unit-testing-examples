package com.andnils.repositories.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.andnils.domain.Car;
import org.springframework.jdbc.core.RowMapper;

public class CarMapper implements RowMapper<Car> {


	@Override
	public Car mapRow(ResultSet resultSet, int rownum) throws SQLException {
		String licensePlate = resultSet.getString("licenseplate");
		String model = resultSet.getString("model");
		int bonus = resultSet.getInt("bonusclass");
		return new Car(licensePlate, model, bonus);
	}
}
