package com.andnils.repositories.mappers;

import com.andnils.domain.Car;
import com.andnils.domain.Customer;
import com.andnils.domain.Rental;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.UUID;

public class PopulatedRentalMapper implements RowMapper<Rental> {
    @Override
    public Rental mapRow(ResultSet resultSet, int rownum) throws SQLException {
		LocalDate fromDate = resultSet.getDate("fromdate").toLocalDate();
		LocalDate toDate = resultSet.getDate("todate").toLocalDate();
        String customer_id = resultSet.getString("customer_id");
        String customer_name = resultSet.getString("customer_name");
        String car_licenseplate = resultSet.getString("car_licenseplate");
        String car_model = resultSet.getString("car_model");
        Customer customer = new Customer(UUID.fromString(customer_id), customer_name);
        Car car = new Car(car_licenseplate, car_model);

        return new Rental(customer, car, fromDate, toDate);
    }
}
