package com.andnils.repositories;

import java.util.List;

import com.andnils.domain.Car;


public interface CarDao {

	Car findByLicensePlate(String licenseplate);

	List<Car> findAllCars();

	// This function isn't really necessary, but
	// we want a separate method so we can mock it later.
	int findBonusClassForCar(Car car);
}
