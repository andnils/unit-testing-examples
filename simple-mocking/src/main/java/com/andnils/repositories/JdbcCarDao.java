package com.andnils.repositories;

import java.util.List;
import javax.sql.DataSource;

import com.andnils.domain.Car;
import com.andnils.repositories.mappers.CarMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcCarDao implements CarDao {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public Car findByLicensePlate(String licenseplate) {
		Car car = jdbcTemplate.queryForObject(
				"SELECT licenseplate, model, bonusclass FROM cars WHERE licenseplate = ?",
				new Object[] {licenseplate},
				new CarMapper());

		return car;
	}


	@Override
	public List<Car> findAllCars() {
		return jdbcTemplate.query("SELECT licenseplate, model, bonusclass FROM cars", new CarMapper());
	}


	@Override
	public int findBonusClassForCar(Car car) {
		return jdbcTemplate.queryForObject("SELECT bonusclass FROM cars WHERE licenseplate = ?",
				new Object[]{car.getLicensePlate()},
				Integer.class);
	}
}
