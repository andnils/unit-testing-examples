package com.andnils.repositories;

import java.util.List;
import javax.sql.DataSource;

import com.andnils.domain.Rental;
import com.andnils.repositories.mappers.PopulatedRentalMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcRentalDao implements RentalDao {


	private JdbcTemplate jdbcTemplate;


	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}


	@Override
	public List<Rental> findAllPopulatedRentals() {
		return jdbcTemplate.query(
				"select rentals.id as id, rentals.fromdate as fromdate, rentals.todate as todate, " +
						"  customers.id as customer_id, customers.name as customer_name, " +
						"  cars.licenseplate as car_licenseplate, cars.model as car_model " +
						"from rentals JOIN customers ON customers.id = rentals.fk_customer " +
						"JOIN cars ON cars.licenseplate = rentals.fk_car",
				new PopulatedRentalMapper());
	}

}
