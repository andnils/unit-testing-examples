
create table customers (
  id varchar(99) primary key,
  name varchar(255)
);



create table cars (
  licenseplate varchar(9) primary key,
  model varchar(255),
  bonusclass INT NOT NULL
);


create table rentals (
  id BIGINT PRIMARY KEY,
  fromdate DATE NOT NULL,
  todate DATE NOT NULL,
  fk_customer varchar(99),
  fk_car varchar(9)
);


ALTER TABLE rentals
    ADD FOREIGN KEY (fk_customer)
      REFERENCES customers (id);


ALTER TABLE rentals
    ADD FOREIGN KEY (fk_car)
      REFERENCES cars (licenseplate);

