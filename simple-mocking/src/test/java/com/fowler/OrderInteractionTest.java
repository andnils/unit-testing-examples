package com.fowler;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OrderInteractionTest {

	private static String TALISKER = "Talisker";

	@Mock
	Warehouse warehouseMock;

	@Test
	public void testFillingRemovesInventoryIfInStock() {
		//setup - data
		Order order = new Order(TALISKER, 50);

		//setup - expectations
		when(warehouseMock.hasInventory(contains(TALISKER), eq(50))).thenReturn(true);

		//exercise
		order.fill(warehouseMock);

		//verify
		verify(warehouseMock).remove(contains(TALISKER), eq(50));
		Assert.assertTrue(order.isFilled());
	}

	public void testFillingDoesNotRemoveIfNotEnoughInStock() {
		Order order = new Order(TALISKER, 51);

		when(warehouseMock.hasInventory(any(), any())).thenReturn(false);

		order.fill(warehouseMock);

		verify(warehouseMock, never()).remove(any(), any());
		assertFalse(order.isFilled());
	}

}
