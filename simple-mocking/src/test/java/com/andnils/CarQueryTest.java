package com.andnils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import com.andnils.domain.Car;
import com.andnils.services.CarService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context.xml")
public class CarQueryTest {

	@Autowired
    private CarService carService;


    @Test
    public void findByLicensePlate() {
        Car byLicensePlate = carService.findByLicensePlate("ABC-123");

        assertEquals("ABC-123", byLicensePlate.getLicensePlate());
    }

    @Test
    public void testfindAllCars() {
        List<Car> cars = carService.findAllCars();

        assertFalse(cars.isEmpty());
    }
}
