package com.andnils.bonus;

import static com.andnils.TestUtil.createRentalForDays;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import com.andnils.domain.Car;
import com.andnils.domain.Rental;
import com.andnils.repositories.CarDao;
import com.andnils.repositories.RentalDao;
import com.andnils.services.RentalService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BonusCalculationTest {

	@Mock
	private RentalDao rentalDao;

	@Mock
	private CarDao carDao;

	@InjectMocks
	private RentalService rentalService;


	@Test
	public void calc_bonus_for_one_day() {

		Rental rental = createRentalForDays(1);

		when(carDao.findBonusClassForCar(any(Car.class))).thenReturn(1);

		long bonusPoints = rentalService.calculateBonusPoints(rental);

		assertEquals(100, bonusPoints);
	}

	@Test
	public void calc_bonus_for_six_days() {

		Rental rental = createRentalForDays(6);

		when(carDao.findBonusClassForCar(any(Car.class))).thenReturn(1);

		long bonusPoints = rentalService.calculateBonusPoints(rental);

		assertEquals(600, bonusPoints);
	}


	@Test
	public void calc_bonus_for_one_week() {

		Rental rental = createRentalForDays(7);

		// a car with bonus class 1 gives you 100 points per day
		// and a weekly extra of 100 points per 7 days
		when(carDao.findBonusClassForCar(any(Car.class)))
				.thenAnswer(invocation -> {
					Car c = (Car) invocation.getArguments()[0];
					// do something with c
					return 1;
				});


		long bonusPoints = rentalService.calculateBonusPoints(rental);

		assertEquals(800, bonusPoints);
	}

}
