package com.andnils.bonus;

import static com.andnils.TestUtil.createRentalForDays;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;

import com.andnils.domain.Car;
import com.andnils.domain.Rental;
import com.andnils.repositories.CarDao;
import com.andnils.repositories.RentalDao;
import com.andnils.services.RentalService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@RunWith(Parameterized.class)
public class ParameterizedBonusCalculationTest {

	@Mock
	private RentalDao rentalDao;

	@Mock
	private CarDao carDao;

	@InjectMocks
	private RentalService rentalService;


	private int bonusClass;
	private int days;
	private int expectedPoints;


	@Parameters(name = "{index}: BonusClass: {0}, Days: {1}, Points: {2}")
	public static Collection<Object[]> testData() {
		return Arrays.asList(new Integer[][]{
				{1, 1, 100},
				{1, 6, 600},
				{1, 7, 800},
				{2, 1, 200},
				{2, 6, 1200},
				{2, 7, 1600}
		});
	}


	public ParameterizedBonusCalculationTest(int bonusClass, int days, int expectedPoints) {
		this.bonusClass = bonusClass;
		this.days = days;
		this.expectedPoints = expectedPoints;

		MockitoAnnotations.initMocks(this);
	}




	@Test
	public void calculateBonus() {
		Rental rental = createRentalForDays(days);

		when(carDao.findBonusClassForCar(any(Car.class))).thenReturn(bonusClass);

		long actualPoints = rentalService.calculateBonusPoints(rental);

		assertEquals(expectedPoints, actualPoints);
	}

}
