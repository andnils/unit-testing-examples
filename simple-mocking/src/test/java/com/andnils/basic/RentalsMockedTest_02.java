package com.andnils.basic;

import static org.junit.Assert.assertTrue;

import com.andnils.repositories.RentalDao;
import com.andnils.services.RentalService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class RentalsMockedTest_02 {

	@Mock
	private RentalDao rentalDao;

	@InjectMocks
	private RentalService rentalService;

	@Before
	public void setup() {
		// initMocks creates annotated mocks (rentalDao),
		// and injects them using constructor or setter.
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void dummyTest() {
		assertTrue(true);
	}
}
