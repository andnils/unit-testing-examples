package com.andnils.basic;

import static org.junit.Assert.assertTrue;

import com.andnils.repositories.RentalDao;
import com.andnils.services.RentalService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RentalsMockedTest_03 {

	@Mock
	private RentalDao rentalDao;

	@InjectMocks
	private RentalService rentalService;

	@Before
	public void setup() {
		// MockitoJUnitRunner takes care of initMocks for us
	}


	@Test
	public void dummyTest() {
		assertTrue(true);
	}
}
