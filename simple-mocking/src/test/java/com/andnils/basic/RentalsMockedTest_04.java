package com.andnils.basic;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Set;

import com.andnils.TestUtil;
import com.andnils.domain.Car;
import com.andnils.domain.Customer;
import com.andnils.domain.Rental;
import com.andnils.repositories.CarDao;
import com.andnils.repositories.RentalDao;
import com.andnils.services.RentalService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@SuppressWarnings("Annotation")
@RunWith(MockitoJUnitRunner.class)
public class RentalsMockedTest_04 {

	@Mock
	private RentalDao rentalDao;

	@Mock
	private CarDao carDao;

	@InjectMocks
	private RentalService rentalService;


	@Test
	public void no_available_cars_when_only_car_is_rented() {

		//////////
		// Arrange

		// There is only one car
		Car theOnlyCar = TestUtil.createRandomCar();
		when(carDao.findAllCars())
				.thenReturn(singletonList(theOnlyCar));

		// and it is rented between 1-30 March
		Rental rental = new RentalBuilder()
				.withCar(theOnlyCar)
				.fromDate(LocalDate.of(2015, 3, 1))
				.toDate(LocalDate.of(2015, 3, 30))
				.build();

		when(rentalDao.findAllPopulatedRentals())
				.thenReturn(singletonList(rental));

		//////
		// Act
		Set<Car> availableCarsOnDate = rentalService.findAvailableCarsOnDate(LocalDate.of(2015, 3, 15));

		/////////
		// Assert
		assertTrue(availableCarsOnDate.isEmpty());
	}


	private static class RentalBuilder {

		private Customer customer;
		private Car car;
		private LocalDate fromDate;
		private LocalDate toDate;


		RentalBuilder() {
			this.customer = TestUtil.createRandomCustomer();
		}


		RentalBuilder withCustomer(Customer c) {
			this.customer = c;
			return this;
		}


		RentalBuilder withCar(Car c) {
			this.car = c;
			return this;
		}


		RentalBuilder fromDate(LocalDate d) {
			this.fromDate = d;
			return this;
		}


		RentalBuilder toDate(LocalDate d) {
			this.toDate = d;
			return this;
		}


		Rental build() {
			return new Rental(customer, car, fromDate, toDate);
		}
	}
}
