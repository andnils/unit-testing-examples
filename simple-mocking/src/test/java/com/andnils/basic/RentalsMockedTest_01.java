package com.andnils.basic;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import com.andnils.repositories.RentalDao;
import com.andnils.services.RentalService;
import org.junit.Before;
import org.junit.Test;

public class RentalsMockedTest_01 {


	private RentalService rentalService;

	@Before
	public void setup() {
		RentalDao mockedDao = mock(RentalDao.class);
		rentalService = new RentalService();
		rentalService.setRentalDao(mockedDao);
	}


	@Test
	public void dummyTest() {
		assertTrue(true);
	}
}
