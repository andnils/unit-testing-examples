package com.andnils;

import static org.junit.Assert.assertFalse;

import java.time.LocalDate;
import java.util.Set;

import com.andnils.domain.Car;
import com.andnils.services.RentalService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context.xml")
public class RentalsQueryTest {

	@Autowired
	private RentalService rentalService;


	@Test
	public void testFindOnDate() throws Exception {
		Set<Car> availableCarsOnDate = rentalService.findAvailableCarsOnDate(LocalDate.now());

		assertFalse("No cars should be available today.", availableCarsOnDate.isEmpty());
	}
}
