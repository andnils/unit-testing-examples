package com.andnils;

import java.time.LocalDate;
import java.util.Random;
import java.util.UUID;

import com.andnils.domain.Car;
import com.andnils.domain.Customer;
import com.andnils.domain.Rental;
import org.apache.commons.lang.RandomStringUtils;

public final class TestUtil {

	/**
	 * Create a Car with a random license plate
	 * in format 'ABC-123', and a random model
	 * name (9 chars).
	 * A bonus class is assigned, 1-3.
	 *
	 * @return a random car
	 */
	public static Car createRandomCar() {
		String licensePlate = String.format("%s-%s",
				RandomStringUtils.randomAscii(3),
				RandomStringUtils.randomNumeric(3));
		String model = RandomStringUtils.randomAscii(9);
		int bonusClass = 1 + new Random().nextInt(3);

		return new Car(licensePlate, model, bonusClass);
	}


	/**
	 * Create a customer with random id and random name.
	 *
	 * @return a customer
	 */
	public static Customer createRandomCustomer() {
		return new Customer(UUID.randomUUID(), RandomStringUtils.randomAscii(12));
	}


	/**
	 * Creates a random rental, containing:
	 * - a random customer,
	 * - a random car,
	 * - rental starts today
	 *
	 * @param numDays number of rental days. End date will be today + numDays.
	 * @return a random Rental
	 */
	public static Rental createRentalForDays(int numDays) {
		LocalDate rentalStartDate = LocalDate.now();
		LocalDate rentalEndDate = rentalStartDate.plusDays(numDays);

		return new Rental(
				createRandomCustomer(),
				createRandomCar(),
				rentalStartDate,
				rentalEndDate);
	}
}
